# Henge

A generation and visualization tool for graphs

---

## Expectations:
- __basic graph generation__
	- [ ] P(n) - Path with n-edges
	- [ ] C(n) - cycle of length n
	- [ ] ...
- __basic operations on graphs__
	- [ ] adding or removing both vertices and edges
	- [ ] merging graphs
- __several forms of graph visualization__
	- [ ] Circular visualization similar to CIRCOS used in genome mapping
	- [ ] HTML5 output
	- [ ] SVG output
	- [ ] animations
	- [ ] usable visualizations with ascii in terminal window.
		- potentailly with several layers of simplifcation
- __ability to specify input and output types__
	- [ ] trivial graph format .tgf
	- [ ] adjacency matrix -> csv, tsv, etc.
	- [ ] compressed or decomposed formats that will require more research

## Data Structure:
- __data:__
	- vertices - string or integer as ID
	- edges - pair of IDs denoting directionality
	- labels - strings that belong to vertices or edges
		- information on coloring?
		- other ways of grouping vertices?
- __potential structures: [^1]__
	- list
		* __\+__ this allows intuitive control over edges and vertices
		* __\-__ not inherantly organized (more computation)
		* __\+__ very efficient use of memory (less memory)
	- dictionary
		* __\+__ seems decent for directed graphs, vertices own their outgoing connections
		* __\-__ assuming driected graph when not nessesary uses more memory and filespace
		* __\+__ simple to find edges originating from any vertex
	- array (adjacency matrix)
		* __\+__ simple to add or remove vertices and edges (less computation)
		* __\-__ storing every lack of connection as well (more memory)
		* __\+__ bidirectionality nearly halves memory usage.	
- __optional directionality__
	- some of these structures become more or less efficient depending on the type of graph

[^1]: _These observations on structure are based on my experience with Python. We have approached similar problems very differently when using C++ implimentations of pointers specifically._

## Current interest on compressing or decomposing
This is interesting to us at the moment for the reason of making both human and machine computation on large graphs faster and more inuitive.

- __decompse graphs into largest complete components:__
For example:  
a,b,c,d form k(4), a is connected to e  
The edges in file might be encoded as:  
>a b c d  
>a e  

Alternatively, decompose graphs into cycles or paths the same way... the most efficient form certainly depends on the structure of the graph.

---

_License: Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International Public License_