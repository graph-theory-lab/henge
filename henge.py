#  -----------------
# |                 |
# |     HENGE       |
# |                 |
#  -----------------
# ~ named after ancient circles of sticks and stones
# author: Bri (they/them)
#============================
# +---------+
# | Path(n) |
# +---------+
#	arguments:
#		filename - string
#		n - positive integer
filename = "output.tgf"
n = 6

# using lists:
vertex = range(n+1)
# directed:
edge = [a,a+1 for a in range(n) ]
edge +=[a+1,a for a in range(n) ]
# bidirected:
edge = [a,a+1 for a in range(n) ]

#-----------------------
# using dictionary:
# directed:
graph = {v:[(v-1), (v+1)] for v in range(n)}
graph[0] = [1]
graph[n+1] = [n]
# bidirected:
graph = {v:[(v+1)] for v in range(n)}
graph[n+1]=None
#----------------------
#using array: (adjacency matrix)
adjacency = [[True if abs(a-b)==1 else False for a in range(n)] for b in range(n)]

with open(filename, 'w') as file:

#=============================
# +----------+
# | Cycle(n) |
# +----------+
#	arguments:
#		filename - string
#		n - positive integer

filename = "output.tgf"
n = 6
#using lists:
vertex = range(n)
edge = [(v,(v+1)%n) for v in range(n)]

with open(filename, 'w') as file:
	for v in vertex:
		file.write(f"{v}\n")
	file.write('#')	#seperates vertices from edges
	for a,b in edge:
		file.write(f"{a} {b}\n")
		# file.write(f"{b} {a}\n") #for bidirectionality

#using dictionary:
graph = { v:[(v-1)%n, (v+1)%n] for v in range(n)}
# graph = { v:[(v+1)%n] for v in range(n)} # for bidirected graphs
with open(filename, 'w') as file:
	for v in vertex:
		file.write(f"{v}\n")
	file.write('#')	#seperates vertices from edges
	for a,e in vertex.items():
		for b in e:
			file.write(f"{a} {b}\n")

#using array: (adjacency matrix)
#directed:
adjacency = [[True if (a-b)%n==1 or (a-b)%n==n-1 else False for a in range(n)] for b in range(n)]
#bidirected pseudograph:
adjacency = [[True if (a-b)%n==1 or (a-b)%n==n-1 else False for a in range(n) if a<=b] for b in range(n)]
#bidirected:
adjacency = [[True if (a-b)%n==1 or (a-b)%n==n-1 else False for a in range(n) if a<b] for b in range(n)]